<?php

declare(strict_types=1);

/*
 * This file is part of Synergy Business Suite Project
 *
 * (c) PT. Synergy Engineering
 */

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231011035050 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE equipment_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE fabrication_rate_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE equipment (id INT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE fabrication_rate (id INT NOT NULL, equipment_id INT NOT NULL, rate VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_E15DB9C8517FE9FE ON fabrication_rate (equipment_id)');
        $this->addSql('ALTER TABLE fabrication_rate ADD CONSTRAINT FK_E15DB9C8517FE9FE FOREIGN KEY (equipment_id) REFERENCES equipment (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP SEQUENCE equipment_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE fabrication_rate_id_seq CASCADE');
        $this->addSql('ALTER TABLE fabrication_rate DROP CONSTRAINT FK_E15DB9C8517FE9FE');
        $this->addSql('DROP TABLE equipment');
        $this->addSql('DROP TABLE fabrication_rate');
    }
}
