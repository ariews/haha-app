const $ = require('jquery');
Window.prototype.$ = window.jQuery = global.$ = global.jQuery = $;

function applyRemoveEvent(btnCollection) {
    btnCollection.each((i, el) => {
        let btn = $(el)
        btn.on('click', (e) => {
            e.preventDefault()
            btn.parents('.rowRate').remove()
        })
    })
}

(function ($) {
    let eqForm = $('form#eq-form')
    let addElementBtn = $('button#add-element')
    let idx = eqForm.data('idx')
    let fRate = eqForm.find('#equipment_fabricationRates')

    applyRemoveEvent(fRate.find('button.remove'));

    addElementBtn.on('click', (e) => {
        e.preventDefault()

        let formPrototype = fRate.data('prototype');
        let row = $(formPrototype.replace(/__name__/g, idx))

        applyRemoveEvent(row.find('button.remove'))

        idx++;
        eqForm.data('idx', idx)
        fRate.append(row)
    })
})(jQuery)
