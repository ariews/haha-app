<?php

declare(strict_types=1);

/*
 * This file is part of Synergy Business Suite Project
 *
 * (c) PT. Synergy Engineering
 */

namespace App\Entity;

use App\Repository\EquipmentRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: EquipmentRepository::class)]
#[Gedmo\SoftDeleteable(fieldName: 'deletedAt', timeAware: false, hardDelete: true)]
class Equipment
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank]
    #[Assert\Length(min: 3)]
    private ?string $name = null;

    #[ORM\OneToMany(mappedBy: 'equipment', targetEntity: FabricationRate::class, cascade: ['persist'], orphanRemoval: true)]
    private Collection $fabricationRates;

    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $deletedAt = null;

    public function __construct()
    {
        $this->fabricationRates = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, FabricationRate>
     */
    public function getFabricationRates(): Collection
    {
        return $this->fabricationRates;
    }

    public function addFabricationRate(FabricationRate $fabricationRate): static
    {
        if (!$this->fabricationRates->contains($fabricationRate)) {
            $this->fabricationRates->add($fabricationRate);
            $fabricationRate->setEquipment($this);
        }

        return $this;
    }

    public function removeFabricationRate(FabricationRate $fabricationRate): static
    {
        if ($this->fabricationRates->removeElement($fabricationRate)) {
            // set the owning side to null (unless already changed)
            if ($fabricationRate->getEquipment() === $this) {
                $fabricationRate->setEquipment(null);
            }
        }

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeImmutable
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeImmutable $deletedAt): static
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }
}
