<?php

declare(strict_types=1);

/*
 * This file is part of Synergy Business Suite Project
 *
 * (c) PT. Synergy Engineering
 */

namespace App\Repository;

use App\Entity\FabricationRate;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<FabricationRate>
 *
 * @method FabricationRate|null find($id, $lockMode = null, $lockVersion = null)
 * @method FabricationRate|null findOneBy(array $criteria, array $orderBy = null)
 * @method FabricationRate[]    findAll()
 * @method FabricationRate[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FabricationRateRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FabricationRate::class);
    }

//    /**
//     * @return FabricationRate[] Returns an array of FabricationRate objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('f')
//            ->andWhere('f.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('f.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?FabricationRate
//    {
//        return $this->createQueryBuilder('f')
//            ->andWhere('f.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
